#!/bin/python3

from browsermobproxy import Server
from selenium import webdriver
from pyvirtualdisplay import Display
import json
import ast
import subprocess, signal
import os
import re
import _thread
from urllib.parse import urlparse
import queue
import uuid


class AdScan:

	def __init__(self):
		self.display = None
		self.proxy = None
		self.driver = None
		self.regex = None
		self.domain_info = {}

	def setup(self):
		def server_setup():
			'''
			Start browsermob proxy server
			Used to collect network call information
			:return: Proxy object
			'''
			print("[+]Starting network proxy...")
			server = Server("/home/mgillespie/Downloads/browsermob-proxy-2.1.4/bin/browsermob-proxy")
			server.start()
			self.proxy = server.create_proxy()
			print("\t[-]Network proxy started")
			return


		def browser_setup():
			'''
			Create Firefox webdriver
			Presets User Agent string
			Disregards SSL warnings
			:return: Driver object
			'''
			print("[+]Starting WebDriver instance...")
			profile = webdriver.FirefoxProfile()
			profile.set_proxy(self.proxy.selenium_proxy())
			profile.set_preference('general.useragent.override',
								   'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36')

			caps = webdriver.DesiredCapabilities.FIREFOX.copy()
			caps['webdriver_accept_untrusted_certs'] = True
			caps['webdriver_assume_untrusted_issuer'] = True

			self.driver = webdriver.Firefox(firefox_profile=profile)
			self.driver.set_page_load_timeout(10)
			print("\t[-]WebDriver started")
			return

		self.display = Display(visible=0, size=(800, 1000))
		self.display.start()
		server_setup()
		browser_setup()
		return


	def load_regex(self, file):
		'''
		Reads advertiser JSON
		:return: Parsed JSON advertiser data
		'''
		print("[+]Loading regex: {}".format(file))
		f = open(file, 'r')
		ads = f.read()
		self.regex = json.loads(ads)
		return



	def analyze(self, domain):
		def regex_url(pos, q, urls):
			'''
			Pattern matcher for ads.json and network call flows
			:param pos: Thread ID
			:param q: Queue object for returning data
			:param urls: Network call flow list
			:return:
			'''
			single_map = self.regex[str(pos)]
			single_map_name = single_map['name']
			single_map_cat = single_map['cat']
			single_map_regex = single_map['regex']
			ad_dict = {}
			for i in urls:
				for j in single_map_regex:
					m = re.search("{}".format(j), i)
					if m:
						ad_dict['name'] = single_map_name
						ad_dict['cat'] = single_map_cat
						beacon = urlparse(i)
						beacon_url = "{0}://{1}".format(beacon.scheme, beacon.netloc)
						ad_dict['beacon'] = beacon_url
						ad_dict['status'] = True
						if beacon.scheme == "https":
							ad_dict['secure'] = True
						else:
							ad_dict['secure'] = False
						q.put(ad_dict)
						return
			q.put(None)
			return

		def regex_map(urls):
			'''
			Builds thread map for regex comparison
			:param urls: Array of URLs seen in network flows
			:return: Array of sorted regex matches
			'''
			adv = []
			print("[+]Building thread pool")
			thread_count = len(self.regex)
			print("\t[-]Threads: {}".format(thread_count))
			print("[+]Parsing regex maps")

			d = []
			threads = []
			q = queue.Queue()
			for i in range(0, thread_count):
				t = _thread.start_new_thread(regex_url, (i, q, urls))
				threads.append(t)

			for i in threads:
				item = q.get()
				d.append(item)

			d = list(filter(None.__ne__, d))
			return d

		def task_id():
			'''
			Random alphanumeric job ID
			:return: 16 character alphanumeric string
			'''
			return (str(uuid.uuid4()).replace("-",""))[0:16]



		print("[+]Targetting: {}".format(domain))
		domain_fqdn = "http://{}".format(domain)
		self.proxy.new_har(domain_fqdn, options={'captureHeaders': True})
		print("[+]Capturing 10s of traffic...")
		try:
			self.driver.get(domain_fqdn)
		except Exception as e:
			pass

		result = json.dumps(self.proxy.har, ensure_ascii=False)
		result = ast.literal_eval(result)
		print("\t[-]Capture complete")
		x = 0
		a = []
		for i in result['log']['entries']:
			a.append(i['request']['url'])
			x += 1
		print("\t[-]Network calls: {}".format(x))
		adv = regex_map(a)
		d = {}
		d['domain'] = domain
		d['ads'] = adv
		self.domain_info[task_id()] = d
		return



	def kill_proxy(self):
		'''
		Searches and kills browsermob proxy PIDs
		Due to known bug in server.stop() and proxy.close() methods
		:return:
		'''
		p = subprocess.Popen(['ps', 'a'], stdout=subprocess.PIPE)
		out, err = p.communicate()
		for line in out.splitlines():
			if 'browsermob' in str(line):
				print("[!]Killing network proxy PID")
				pid = int(line.split(None, 1)[0])
				os.kill(pid, signal.SIGKILL)
				print("\t[!]PID killed")

	def kill_driver(self):
		'''
		Terminates Selenium WebDriver
		:return:
		'''
		print("[!]Killing WebDriver instance")
		self.driver.quit()
		print("\t[!]WebDriver killed")
		return

	def kill_display(self):
		'''
		Terminates VirtualDisplay Bypass
		:return:
		'''
		print("[!]Killing Display bypass")
		self.display.stop()
		print("\t[!]Bypass killed")
		return

	def shutdown(self):
		self.kill_proxy()
		self.kill_driver()
		self.kill_display()
