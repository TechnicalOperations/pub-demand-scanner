## Pub Demand Scanner
Network call flow analyzer for identifying advertiser trackers/beacons.

## Requirements
* Selenium WebDriver
* BrowserMob Proxy
* PyVirtualDisplay (optional)
* Python3

## Included Tracker Files
Default tracker file is ads.json.
This file contains name, category, and regex tracking information.

|Name|Entries  |
|--|--|
|ads_14k.txt|14012  |
|ads_2k.txt|2662|
ads.json|27|


## Usage
    from adscan import AdScan
    x = AdScan()
    x.setup()
    x.load_regex(<FILE>)
    x.analyze(<DOMAIN>)
    x.shutdown()

Domain information stored via x.domain_info
Regex information stored via x.regex

## Note
BrowserMob server configuration MUST point to pre-installed proxy binary
EXAMPLE:

     server = Server("<PATH_TO_BROWSERMOB>/bin/browsermob-proxy")
